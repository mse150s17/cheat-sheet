Eric added his name

### Prof. Jankowski's bash-fu ###
The following combination of git log, loops, and pipes, prints out the number of commits each person made in your repository. Note that if you made commits from two or more machines/accounts, your name may show up in two or more formats (e.g.: "Prof. J" and "Eric Jankowski")

$ git log --format='%aN' | sort -u | while read -r line; do echo $line; git log --author="$line" --oneline | wc -l; done


### Some notes from the second day of class ###
pwd = present working directory

 / slashs - delimiter (characters that separate things
All directories begin with /

/bin - binary files (programs)
/home or /users - users' personal files

$ cd - change directory

$ ls  -  list the files in a directory--files in the present working directory

$ -A - 'flag' (A can be any letter) Flags change the output of commands

$ -l 'flag' is the long form

"man ls" to access manual
Tab completion is the fastest way to type. Start with the first letters of a file, directory, or program and press tab. This will complete it for you. If there are multiple files with the similar names you will need to double tap tab. This will give a list of all files, drectories, or programs containing what you typed.

up and down arrow wil replay old commands

. file = directory we are in 
.. file = directory that our directory is in 

To move up one directory we can use the command:
    $cd ../

Up arrow brings up the last command you used. You can repeat this as many times as you would like.ls

$ history = shows all previous commands 

$ clear = clears the screen to make it look nicer 

$ :q #how to leave a file WITHOUT SAVING
vim is just a command
vim is a text editor interface expressed via the command line 
$ cat #prints output of file onto terminal screen (standard output)
can print more than one file at a time ($ cat directory/filename1 directory/filename2)
Order matters for cat command

$ mkdir name #make a directory
If you name a file with a . in front, it becomes hidden. These directories or files will not show up using the normal $ ls command
$ ls -a #lists all files starting with a dot  (also known as hidden files/directories)

To make your own file you use the vim command:
    $vim FILENAME #this will make a file, and bring you into it. Simply type what you want and save 
Within vim text editor: 
$ :w # saves the file ("writes" the file)
$ :q # exits the file 
$ :wq # saves and exits
$ :q! # forceful exit 
$ :wq! # forceful save and exit # This won't actually save the file, if you don't have permissions in the directory you're trying to save.

display variable value with  echo $VariableName

$PATH = environmental variable that tells shell which directories to search for executable files
(specific to each user) 

$ rm FILENAME #This is the command for removing files.
$ rm -r DIRECTORYNAME #This is the command to remove a directory

Removing is permanent. You can't go to the recycle bin and get it back. Be careful.
If you want to rename a file you need the command:
    $ mv FILENAME NEWFILENAME #Takes your first file and renames it using your second arguement
    Keep in mind: If you mv one file to the same name as an existing file it will overwrite it

Similarly, using the mv command helps you move files
$mv FILENAME TARGETDIRECTORY #You might need to move directories using the ../ 
$ mv  /DIRECTORY/FILENAME .   #moves file to current directory you are in (space between filename and period) 

Tilde (~) automaticall brings you to your home directory

$ cp FILENAME NEWFILENAME #copies a file

When attempting to push commits to bit bucket, make sure you do the following:
-Add the edited file and commit it
-do a "git pull" to get the latest version of the repository
-if a merge is requested, save the merge (":wq" when in vim) 
-after the merge has been successfully executed, do a "git push"
-if the push fails, check back on the files you edited and look for conflicts(i.e. random numbers, random symbols, repeated lines, etc.)
-repeat the add and commit process once conflicts are resolved
-do a "git pull" to update your repository
-save/allow and merge requests
-finish with a git push
If more conflicts occur, double check there are no random characters in the files you edited and always make sure to request a "git pull" before you push.

## Helpful Vim Commands ##

* $ :! = executes external command
* $ :!dir = shows directory listing
* $ :!rm FILENAME = removes file FILENAME 
* $ :r !dir = reads output of dir command and puts it below cursor position
* $ :r = retrieves disk file FILENAME and puts it below cursor position
* $ :s = substitute command 
* $ :w = FILENAME writes current Vim file to disk with name FILENAME
* $ ? = Search for phrase in backward direction 
* $ hls = highlight all matching phrases 
* $ ic = ignore upper/lower case when searching
* $ is = show partial matches for search phrase 
* $ / = search 
* $ % = finds matching ) , ] , or }
* $ 0 = move to start of line
* $ 2w = moves cursor two words forward 
* $ 3e = move cursor to end of third word forward
* $ A = append text 
* $ a = append text after cursor
* $ c$ = change to end of a line 
* $ ce = change from cursor to end of word 
* CTRL-G = show location
* CTRL-O = Go back to where you came from 
* CTRL-R = undo the undo’s 
* $ d$ = delete to end of line
* $ dd = delete whole line
* $ dw = delete word 
* $ G = move to bottom of file 
* $ gg = move to start of file 
* $ i = insert text
* $ j$ = move cursor to end of next line 
* $ n = search for same phrase 
* $ N = search for same phrase in opposite direction
* $ o = open line below cursor and place in Insert mode
* $ O = Open up line above cursor and place in Insert mode 
* $ p = put back text just deleted
* $ r = replace 
* $ R = replace more than one character 
* $ U = undo all changes on a line 
* $ u = undo previous actions 
* $ v motion = visually selects lines in file FILENAME
* $ y = yank command 
 
### Git Commands  

* $ git init = initialize repository 
* $ git status = shows status of repository 
* $ git add = puts files in staging area
* $ git commit = saves staged content as a new commit in the local repository 
* $ git checkout -- FILENAME = if you mess up, replace the changes in your working tree with the last content in head

### **To work remotely using Windows:** ###
Download MobaXterm at mobaxterm.mobatek.net. Install “Home Edition v10.2 (Installer edition)

The VPN system uses the “Cisco AnyConnect” client:

	* If on campus: https://bsuvpn-oncampus.boisestate.edu
	* If off campus: https://bsuvpn-offcampus.boisestate.edu

To connect:

	* Keep the “Group” as the default option
	* Login with your myBoiseState credentials
	* Read and accept the Computer Usage Policy by clicking continue
	* You will be prompted to download the Cisco AnyConnect VPN Client
	* Download and install the software
	* Cisco AnyConnect will be available in your programs menu
		* Once open: click “connect”
		* Enter your myBoiseState password
		* You will have access to the coen-bascottie server as long as you remain connected

You now have all the necessary software to access the server remotely:

	* Open MobaXterm
	* Click “Start local terminal” which will open a command shell
	* Type: ssh yourBSULoginName@coen-bascottie.boisestate.edu 
		* Example: ssh jimmyjoe@coen-bascottie.boisestate.edu

For questions about general connectivity: 

* Ben Peterson benjaminpeterson@boisestate.edu

For questions about the coen-bascottie server:

* Jason Cook jasoncook@boisestate.edu